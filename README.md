# README #

This a script to generate promo codes to use with MAF app

### Preparations ###
* Create rtdb-service-account.json
    * Use this service account: https://console.developers.google.com/iam-admin/serviceaccounts/details/109788236078164953602/keys?project=musopia-apps-testbed
    * Generate a new key as a JSON file
    * Rename it to rtdb-service-account.json and put to the root 
* Create api-key.json
    * Open Firebase project settings: https://console.firebase.google.com/project/musopia-apps-testbed/settings/general/android:net.musopia.fourchordsandroidpro 
    * Copy "Web API key" value
    * Create api-key.json in the root folder and put this content: 
      {
          "api_key": "< Web api key here >"
      }
* Install Node, version 18 or higher
* Run "npm install" from the project root

### How to run
* Run from command line to see usage: `node PromoCodes.js`

### Editing
* I recommend to use Web Storm IDE, it has everything
    
### Who do I talk to? ###

* valeriy.lubnin@musopia.net
