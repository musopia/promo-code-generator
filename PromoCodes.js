'use strict';

const axios = require('axios')
const admin = require("firebase-admin");

const serviceAccount = require("./keys/rtdb-service-account.json");
const apiKey = require("./keys/api-key.json");
const databaseURL = "https://musopia-dev-8d384.firebaseio.com";
const dbVersion = "v2";

let AllowedChars = "123456789ABCDEFGHJKLMNPQRSTUVWXYZ".split('');

let dynamicLinkTemplate = "https://justinguitar.page.link/?link=${link}&apn=net.musopia.fourchordsjustin&isi=1176125504&ibi=net.musopia.fourchordsjustin";
let appUrlTemplate = "https://musopia.net/justinguitar/Code/${path}?displayLoader=true&waitForLogin=true";

const dynamicLinkLength = "UNGUESSABLE";
let badWords = [];

function randomString(length) {
    let result = '';
    while (result.length < length) {
        let i = Math.floor(Math.random() * AllowedChars.length);
        result += AllowedChars[i];
    }
    return result;
}

function randomCode(length) {
    while (true) {
        let code = randomString(length);
        let codeLower = code.toLowerCase();
        let found = false;
        for(let i in badWords) {
            if (codeLower.indexOf(badWords[i]) > -1) {
                found = true;
                break;
            }
        }
        if (!found) return code;
    }
}

async function getPromoCode(db, path) {
    const ref = db.ref(`codes/${dbVersion}/${path}`);
    return (await ref.once("value")).val();
}

async function generatePromoCode(db, path, record, group, meta) {

    var ref = db.ref(`codes/${dbVersion}/${path}`);
    if ((await ref.once("value")).val() == null) {
        await ref.set(record);
        await ref.child('group').set(group);
        if (meta) {
            await ref.child('meta').set(meta);
        }
        await ref.child('info/created').set(new Date().toISOString());
    } else {
        throw new Error(`${path} already exists`);
    }
}

async function updateLink(db, path, link) {
    let admin = db.ref(`codes/${dbVersion}/${path}/info`);
    await admin.child('link').set(link);
}

async function cleanUpPromoCode(db, path) {
    let admin = db.ref(`codes/${dbVersion}/${path}`);
    await admin.child('uids').remove();
    await admin.child('count').remove();
    await admin.child('info/link').remove();
}


async function generateDynamicLink(path, linkRequestBody) {

    linkRequestBody.longDynamicLink = linkRequestBody.longDynamicLink.replace('${path}', path);
    linkRequestBody.longDynamicLink = linkRequestBody.longDynamicLink.replace('$%7Bpath%7D', path);
    linkRequestBody.suffix = {
        option: dynamicLinkLength
    };

    let url = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${apiKey.api_key}`;
    let result = await axios.post(url, linkRequestBody);
    return result.data.shortLink;
}

async function generateBatch(db, folder, codeParam, codeLength, number, group, meta, recordTemplate, createLinks, linkTemplate) {

    let index = 0;
    let retries = 0;
    let code;
    while (index < number) {
        if (retries > 3) {
            throw new Error("Too many retries");
        } else if (retries > 0) {
            console.log("Retrying..");
        }

        switch (codeParam) {
            case "numeric":
                code = (index + 1);
                break;
            case "random":
                code = randomCode(codeLength)
                break;
            default:
                code = codeParam;
                break;
        }

        let path = code;
        if (folder) path = folder + '/' + path;

        try {
            let newRecord = Object.assign({}, recordTemplate);
            await generatePromoCode(db, path, newRecord, group, meta);
            await cleanUpPromoCode(db, path);
        } catch (error) {
            console.log(error.message);
            retries++;
            continue;
        }

        let link = "no_link";
        if (createLinks) {
            let linkRequestBody = Object.assign({}, linkTemplate);
            link = await generateDynamicLink(path, linkRequestBody);

            await updateLink(db, path, link);
        }

        console.log(`${index + 1}, ${link}, ${path}`);
        index++;
    }
}

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    // The database URL depends on the location of the database
    databaseURL: databaseURL
});

let db = admin.database();

function PrintUsage() {
    let usage = "\
        Usage: \n\
        node PromoCodes.js <command> [param=value] .. \n\
        \n\
        Commands: \n\
            generate - code generation \n\
            query - query odes from RTDB\n\
            delete - delete codes from RTDB\n\
        \n\
        'generate' command parameters:\n\
        template - existing code to use a template \n\
        code - values: \n\
            random - generate random string \n\
            numeric - generate sequential numbers \n\
            otherwise it's new code specific name \n\
        length - (optional) length of the random code, default is 8 \n\
        folder - (optional) folder to generate codes in \n\
        number - (optional) number of codes to generate \n\
        link - (optional) 'true' to generate links not just codes \n\
        group - (optional) free form string to tag the batch \n\
        meta - (optional) free form string to add meta data to the code/batch \n\
        \n\
        'query' and 'delete' command parameters:\n\
        folder - (optional) folder to search codes in \n\
        group - (optional) free form string to tag the batch \n\
        dry - (optional) dry run for 'delete' \n\
        \n\
        Examples:\n\
        node PromoCodes.js generate template=TESTCODE1 code=ANOTHERCODE \n\
        node PromoCodes.js generate template=TESTCODE1 code=ANOTHERCODE number=100 link=true group=\"Some influencer batch\" \n\
        node PromoCodes.js query group=\"some_group\" \n\
        node PromoCodes.js query group=\"some_group\" folder=JUSTIN1 \n\
        node PromoCodes.js delete group=\"some_group\" \n\
        node PromoCodes.js delete group=\"some_group\" folder=JUSTIN1 \n\
        \n\
         ";
    console.log(usage);
    process.exit(2);
}

function exitWithError(error) {
    console.log(`Error: \n\n${error}`);
    process.exit(2);
}

function param(name, defaultValue = null) {
    for(const param of process.argv) {
        if (param.startsWith(name + "=")) {
            return param.substring((name + "=").length);
        }
    }
    return defaultValue;
}

function readFileLines(filename) {
    let fs = require("fs")
    let lines = fs.readFileSync(filename)
        .toString('UTF8')
        .split('\r\n');
    for(let i in lines) {
        lines[i] = lines[i].trim().toLowerCase();
    }
    return lines;
}

async function generateCommand() {

    badWords = readFileLines('badwords.txt');

    let codeForTemplate = param('template');
    let folder = param('folder');
    let code = param('code', 'random');
    let number = param('number', 1);
    let createLinks = param('link', false);
    let group = param('group');
    let meta = param('meta');
    let codeLength = param('length', 8);

    if (!codeForTemplate) exitWithError("template param is required.")
    if (code === "numeric" && !folder) exitWithError("Can't generate numeric codes without a folder.")

    var link = encodeURI(appUrlTemplate);
    let linkTemplate = {
        longDynamicLink: dynamicLinkTemplate.replace('${link}', link)
    };

    let recordTemplate = await getPromoCode(db, codeForTemplate);
    if (!recordTemplate) exitWithError(`template not found: ${codeForTemplate}`)

    await generateBatch(db, folder, code, codeLength, number, group, meta, recordTemplate, createLinks, linkTemplate);
}

async function queryCommand() {

    let folder = param('folder');
    let group = param('group');

    let ref = db.ref(`codes/${dbVersion}`);
    if (folder) ref = ref.child(folder);

    let snapshot;
    if (group) {
        snapshot = await ref.orderByChild('group').equalTo(group).once('value');
    } else {
        snapshot = await ref.once('value');
    }
    snapshot.forEach((childSnapshot) => {
        let key = (folder ? folder + "/": "") + childSnapshot.key;
        console.log(key + ", " + (childSnapshot.val().group ?? ""));
    });
    console.log();
    console.log(`Total: ${snapshot.numChildren()}`);
}

async function deleteCommand() {
    let folder = param('folder');
    let group = param('group');
    let dry = param('dry');
    let code = param('code');

    let ref = db.ref(`codes/${dbVersion}`);
    if (folder) ref = ref.child(folder);

    if (code) {
        if (code && group) console.log("'group' is ignored when together with 'code'");
        ref = ref.child(code);
        if ((await ref.once("value")).val() == null) {
            exitWithError("Code not found");
        }
        if (!dry) {
            await ref.remove();
        }
        let displayKey = (folder ? folder + "/": "") + code;
        console.log(`${displayKey} DELETED` + (dry?" (dry run)":""));

        return;
    }

    if (!folder && !group) exitWithError("Can't delete all from root folder.");

    let snapshot;
    if (group) {
        snapshot = await ref.orderByChild('group').equalTo(group).once('value');
    } else {
        snapshot = await ref.once('value');
    }
    let items = [];
    snapshot.forEach((childSnapshot) => {
        items.push({key: childSnapshot.key, group: childSnapshot.val().group});
    });

    for(let i in items) {
        let item = items[i];
        let displayKey = (folder ? folder + "/": "") + item.key;
        if (!dry) {
            await ref.child(item.key).remove();
        }
        console.log(displayKey + ", " + (item.group ?? "") + " - DELETED" + (dry?" (dry run)":""));
    }

    console.log();
    console.log(`Deleted in total: ${snapshot.numChildren()}` + (dry?" (dry run)":""));
}

async function reportCommand() {
    let folder = param('folder');
    let group = param('group');

    let ref = db.ref(`codes/${dbVersion}`);
    if (folder) ref = ref.child(folder);

    let v = (await ref.once("value")).val();
    if (v != null) {
        let keysCount = 0;
        let uidsCount = 0;
        if (group) {
            for (let key in v) {
                if (v[key].group == group) {
                    keysCount++;
                    if (v[key].hasOwnProperty('uids')) {
                        uidsCount++;
                    }
                }
            }
        } else {
            for (let key in v) {
                keysCount++;
                if (v[key].hasOwnProperty('uids')) {
                    uidsCount++;
                }
            }
        }

        console.log(`Redemption report:\n`);
        console.log(`Total codes: ${keysCount}`);
        console.log(`Redeemed: ${uidsCount}`);
    } else {
        exitWithError("Empty folder")
    }
}

async function main() {
// parse params

    if (process.argv.length < 3) {
        PrintUsage();
    }

    let command = process.argv[2];
    switch (command) {
        case 'generate':
            await generateCommand();
            break;
        case 'query':
            await queryCommand();
            break;
        case 'report':
            await reportCommand();
            break;
        case 'delete':
            await deleteCommand();
            break;
        default:
            PrintUsage();
    }
}

main().then(() => {
    setTimeout(() => {
        // let file write finish
        console.log("Finished\n");
        process.exit(0);
    }, 1000);
}).catch(err => {
    exitWithError(err)
});

